<?php
$message='';
$sample_id = $_GET['id'];

$query_result = $obj_sample->edit_sample_info_by_id($sample_id);
$sample_info = mysqli_fetch_assoc($query_result);
extract($sample_info);

if (isset($_POST['btn'])) {
    $obj_sample->update_sample_info_by_id($_POST);
}
?>




<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <p class="text-center text-success lead">Update Sample Info</p>
                <h3 class="text-center text-success lead"></h3>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" action="" method="post">
                    <div class="form-group">
                        <label class="control-label col-lg-3">sample_signature</label>
                        <div class="col-lg-9">
                            <input type="text" name="sample_signature" value="<?php echo $sample_signature;?>" class="form-control" required>
                            <input type="hidden" name="id" value="<?php echo $id; ?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">model_id</label>
                        <div class="col-lg-9">
                            <input type="number" name="model_id" value="<?php echo $model_id;?>" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">in_date</label>
                        <div class="col-lg-9">
                            <input type="date" name="in_date" value="<?php echo $in_date;?>" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3"> purpose</label>
                        <div class="col-lg-9">
                            <input type="text" name="purpose" value="<?php echo $purpose;?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">remarks</label>
                        <div class="col-lg-9">
                            <textarea name="remarks" class="form-control"><?php echo $remarks;?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-9">
                            <input type="submit" name="btn" value="Update Sample Info" class="btn btn-primary btn-block">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>