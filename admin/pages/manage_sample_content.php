<?php
$message = '';

$query_result = $obj_sample->select_all_sample_info();
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header text-center text-success">
            <?php echo $message; ?>
            <?php
            if (isset($_SESSION['message'])) {
                echo $_SESSION['message'];
                unset($_SESSION['message']);
            }
            ?>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading text-center lead">
                All Sample Information Goes Here
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>sample_signature</th>
                            <th>model_id</th>
                            <th>in_date</th>
                            <th>purpose</th>
                            <th>remarks</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        while ($all_sample_info = mysqli_fetch_assoc($query_result)) {
                            extract($all_sample_info);
                            ?>
                            <tr class="odd gradeX">
                                <td> <?php echo $id; ?> </td>
                                <td><?php echo $sample_signature; ?></td>
                                <td><?php echo $model_id; ?></td>
                                <td><?php echo $in_date; ?></td>
                                <td> <?php echo $purpose; ?> </td>
                                <td> <?php echo $remarks; ?> </td>
                               
                                
                                <td class="center">

                                    <a href="edit_sample.php?id=<?php echo $id; ?>" class="btn btn-success" title="Edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </a>
                                    <a href="?status=delete&&id=<?php echo $id; ?>" class="btn btn-danger" title="Delete" onclick="return check_delete_status();">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
                
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>