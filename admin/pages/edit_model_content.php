<?php
$message='';
$id_model = $_GET['id'];

$query_result = $obj_models->edit_model_info_by_id($id_model);
$model_info = mysqli_fetch_assoc($query_result);
extract($model_info);

if (isset($_POST['btn'])) {
    $obj_models->update_model_info_by_id($_POST);
}
?>

?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <p class="text-center text-success lead">Add Project Owner Form</p>
                <h3 class="text-center text-success lead"><?php echo $message; ?></h3>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" action="" method="post">
                    <div class="form-group">
                        <label class="control-label col-lg-3">name</label>
                        <div class="col-lg-9">
                            <input type="text" name="name" value="<?php echo $name;?>" class="form-control" required>
                        <input type="hidden" name="id_model" value="<?php echo $id_model; ?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">phone_type</label>
                        <div class="col-lg-9">
                            <input type="text" name="phone_type" value="<?php echo $phone_type;?>" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">product_owner_id</label>
                        <div class="col-lg-9">
                            <input type="number" name="product_owner_id" value="<?php echo $product_owner_id;?>" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3"> supplier_id</label>
                        <div class="col-lg-9">
                            <input type="number" name="supplier_id" value="<?php echo $supplier_id;?>" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Spec_and_NPD</label>
                        <div class="col-lg-9">
                            <input type="text" name="Spec_and_NPD" value="<?php echo $Spec_and_NPD;?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">po_date</label>
                        <div class="col-lg-9">
                            <input type="date" name="po_date" value="<?php echo $po_date;?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">Launching_Year</label>
                        <div class="col-lg-9">
                            <input type="text" name="Launching_Year" value="<?php echo $Launching_Year;?>" class="form-control">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-9">
                            <input type="submit" name="btn" value="Update model Info" class="btn btn-primary btn-block">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>