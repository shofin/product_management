<?php
   
   $message='';
   
   if(isset($_POST['btn'])) {
       $message=$obj_sample->save_sample_info($_POST);
   }
   
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <p class="text-center text-success lead">Add New Sample</p>
                <h3 class="text-center text-success lead"><?php echo $message; ?></h3>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" action="" method="post">
                    <div class="form-group">
                        <label class="control-label col-lg-3">sample_signature</label>
                        <div class="col-lg-9">
                            <input type="text" name="sample_signature" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">model_id</label>
                        <div class="col-lg-9">
                            <input type="number" name="model_id" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">in_date</label>
                        <div class="col-lg-9">
                            <input type="date" name="in_date" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3"> purpose</label>
                        <div class="col-lg-9">
                            <input type="text" name="purpose" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">remarks</label>
                        <div class="col-lg-9">
                            <textarea name="remarks" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-9">
                            <input type="submit" name="btn" value="Save Sample Info" class="btn btn-primary btn-block">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>