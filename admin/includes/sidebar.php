

<!--product Management Control panel-->

<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
                <!-- /input-group -->
            </li>
            <li>
                <a href="admin_master.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Manpower Control Panel <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="manage_manpower.php"> Manage Manpower </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Supplier Control Panel <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="manage_supplier.php"> Manage Supplier </a>
                    </li>
                    
                </ul>
            </li>

            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Models Info <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="add_model.php"> Add New Model </a>
                    </li>
                    <li>
                        <a href="manage_model.php"> Manage Models </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Sample Info <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="add_sample.php"> New Sample </a>
                    </li>
                    <li>
                        <a href="manage_sample.php"> Manage Sample </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>